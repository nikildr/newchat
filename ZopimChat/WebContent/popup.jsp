<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link class="jsbin" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.min.css" rel="stylesheet" type="text/css" />
  <script class="jsbin" src="http://code.jquery.com/jquery-1.9.1.js"></script>
  <script class="jsbin" src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
  <script type="text/javascript" src="js/jquery.dialogextend.js"></script>
<script type="text/javascript">
$(function(){
	  $("#my-button").click(function(){
	    $("#content")
	      .dialog({
	        "title" : "This is dialog title",
	        "buttons" : { "OK" : function(){ $(this).dialog("close"); } }
	       }).dialogExtend({
	        "close" : true,
	        "maximize" : true,
	        "minimize" : true,
	        "dblclick" : "collapse",
	        "titlebar" : "transparent",
	        "icons" : {
	          "close" : "ui-icon-circle-close",
	          "maximize" : "ui-icon-circle-plus",
	          "minimize" : "ui-icon-circle-minus",
	          "restore" : "ui-icon-bullet"
	        },
	        "events" : {
	          "load" : function(evt, dlg){ alert(evt.type+"."+evt.handleObj.namespace); },
	          "maximize" : function(evt, dlg){ alert(evt.type+"."+evt.handleObj.namespace); },
	          "minimize" : function(evt, dlg){ alert(evt.type+"."+evt.handleObj.namespace); },
	          "restore" : function(evt, dlg){ alert(evt.type+"."+evt.handleObj.namespace); }
	        }
	       
	      });
	  });​
</script>
</head>
<body>
<button id="my-button">clickMe!</button>
<div id="content">hai..</div>
</body>
</html>