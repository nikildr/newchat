<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<title>Facebook Login JavaScript Example</title>
<meta charset="UTF-8">
<!-- <script type="text/javascript" src="js/facebook.js"></script> -->
</head>
<body>
<script>
  // This is called with the results from from FB.getLoginStatus().
  function statusChangeCallback(response) {
  /*  console.log('statusChangeCallback');
   console.log(response); */
    // The response object is returned with a status field that lets the
    // app know the current login status of the person.
    // Full docs on the response object can be found in the documentation
    // for FB.getLoginStatus().
    if (response.authResponse) {
    
      // Logged into your app and Facebook.
       document.getElementById('details').innerHTML = "<span>"
       +"<fb:profile-pic uid='loggedinuser' facebook-logo='true'</fb:profile-pic>"
       +"Welcome, <fb:name uid='loggedinuser' useyou='false'></fb:name>"
       +"you are Signed in through your facebook account </span>";
      console.log(response.authResponse.accessToken);
      testAPI();
    } else if (response.status === 'not_authorized') {
      // The person is logged into Facebook, but not your app.
      document.getElementById('userdetails').innerHTML = 'Please log ' +
        'into this app.';
    } else {
      // The person is not logged into Facebook, so we're not sure if
      // they are logged into this app or not.
      /*  document.getElementById('details').innerHTML = 'Please log ' +
        'into Facebook.';  */
    }
  }

  // This function is called when someone finishes with the Login
  // Button.  See the onlogin handler attached to it in the sample
  // code below.
  function checkLoginState() {
    FB.getLoginStatus(function(response) {
    
      statusChangeCallback(response);
    });
  }

  window.fbAsyncInit = function() {
  FB.init({
    appId      : '279753312198447',
    cookie     : true,  // enable cookies to allow the server to access 
                        // the session
    xfbml      : true,  // parse social plugins on this page
    status     : true,
    oauth      : true,
    version    : 'v2.0' // use version 2.0
  });

  // Now that we've initialized the JavaScript SDK, we call 
  // FB.getLoginStatus().  This function gets the state of the
  // person visiting this page and can return one of three states to
  // the callback you provide.  They can be:
  //
  // 1. Logged into your app ('connected')
  // 2. Logged into Facebook, but not your app ('not_authorized')
  // 3. Not logged into Facebook and can't tell if they are logged into
  //    your app or not.
  //
  // These three cases are handled in the callback function.

  FB.getLoginStatus(function(response) {
		 document.getElementById('userdetails').innerHTML = "<span>"
  	       +"<fb:profile-pic uid='loggedinuser' facebook-logo='true'</fb:profile-pic>"
  	       +"Welcome, <fb:name uid='loggedinuser' useyou='false'></fb:name>"
  	       +"you are Signed in through your facebook account </span>";
    statusChangeCallback(response);
  });

  };

  // Load the SDK asynchronously
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

  // Here we run a very simple test of the Graph API after login is
  // successful.  See statusChangeCallback() for when this call is made.
  function testAPI() {
    console.log('Welcome!  Fetching your information.... ');
    FB.api('/me', function(response) {
    	 alert(response.name);
    	console.log(JSON.stringify(response));
      console.log('Successful login for: ' + response.name);
      document.getElementById('details').innerHTML =
        'Thanks for logging in, ' + response.name + '!';
    });
  }
</script>

<!--
  Below we include the Login Button social plugin. This button uses
  the JavaScript SDK to present a graphical Login button that triggers
  the FB.login() function when clicked.
-->

<fb:login-button  data-size="icon"  onlogin="checkLoginState();" scope="public_profile,email">
</fb:login-button>

<div id="fb-root"></div>
<div id="userdetails">
</div>
<!-- <div
  class="fb-like"
  data-send="true"
  data-width="450"
  data-show-faces="true">
</div> -->
</body>
</html>