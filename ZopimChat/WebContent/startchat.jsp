<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>kenscio chat</title>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="css/chat.css" rel="stylesheet" type="text/css">

<script type="text/javascript">
$(document).ready(function(){
	$("#firstheader").hide();
	
	$(".minimize").click(function(){
		$("#secondheader").hide();
		$("#firstheader").show();
	});
	$("#firstheader").click(function(){
		$("#secondheader").show();
		$("#firstheader").hide();
	});
});


</script>
</head>
<body>
<div id="firstheader">
<img src="img/Bubble.png" id="icon">
<label id="firstlabel">Kenscio Chat</label>
</div>
<div id="secondheader">
<div id="top">
  <label id="secondlabel">Kenscio Live Chat</label><button type="button" class="close"></button>
  <button type="button" class="minimize" ></button></div>
  <div id="chatcontent">
  </div>
  <div >   
  <textarea rows="2" cols="37" class="chattext"></textarea>
  </div>
  </div>
</body>
</html>