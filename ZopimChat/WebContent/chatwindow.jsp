<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<link type="text/css" rel="stylesheet" href="css/chat.css"></link>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/facebook.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $("#secondheader").hide();
    $("#nameshow").hide();
    
    $("#firstheader").click(function(){
    	  $("#secondheader").show();
    	  //$("#secondheader").css("height","260px");
    	  
    	  $("#firstheader").hide();
    });
    $("#min").click(function(){
    	  $("#secondheader").hide();
    	  $("#firstheader").show();
    });
    $("#fakeclick").click(function(){
    	 //$("#secondheader").css("height","325px");
    	$("#nameshow").show();
    	$("#fakeinput").hide();
    });
});

function verifydata(){
	var name=$("#name").val();
	var email=$("#email").val();
	var department=$("#department").val();
	
	if(name==""||name==null||name.length==0){
		  $("#name").css("border","1px solid #a8281f");
		  $("#name").attr("placeholder","Name is Required","color","#a8281f");
		  return false;
		}
	 else{
		  $("#name").css("border","1px solid #DDD");
	  }
	 if(email==""||email==null||email.length==0){
		  $("#email").css("border","1px solid #a8281f");
		 $("#email").attr("placeholder","Email is Required","color","#a8281f");
		  return false;
	 }
	 else{
		  $("#email").css("border","1px solid #DDD");
	 }
	 var emailpattern=/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	 if(!emailpattern.test(email)){
		 $("#email").css("border","1px solid #a8281f");
		   alert("Please Enter Valid Email ID");
		  return false;
	 }
	 else{
		 
			  $("#email").css("border","1px solid #DDD");
	
	 }
}
</script>
</head>
<body>
<script type="text/javascript">
function statusChangeCallback(response) {
    if (response.authResponse) {
    	 document.getElementById('details').innerHTML = "<span>"
    	       +"<fb:profile-pic uid='loggedinuser' facebook-logo='true'</fb:profile-pic>"
    	       +"Welcome, <fb:name uid='loggedinuser' useyou='false'></fb:name>"
    	       +"you are Signed in through your facebook account </span>";
    	      console.log(response.authResponse.accessToken);
    	      testAPI();
    	    } else if (response.status === 'not_authorized') {
    	      // The person is logged into Facebook, but not your app.
    	      document.getElementById('userdetails').innerHTML = 'Please log ' +
    	        'into this app.';
    	    } else {
    	      // The person is not logged into Facebook, so we're not sure if
    	      // they are logged into this app or not.
    	      /*  document.getElementById('details').innerHTML = 'Please log ' +
    	        'into Facebook.';  */
    	    }
    	  }
function checkLoginState() {
    FB.getLoginStatus(function(response) {
    
      statusChangeCallback(response);
    });
  }

  window.fbAsyncInit = function() {
  FB.init({
    appId      : '279753312198447',
    cookie     : true,  // enable cookies to allow the server to access 
                        // the session
    xfbml      : true,  // parse social plugins on this page
    status     : true,
    oauth      : true,
    version    : 'v2.0' // use version 2.0
  });
  };
  FB.getLoginStatus(function(response) {
		 document.getElementById('userdetails').innerHTML = "<span>"
	       +"<fb:profile-pic uid='loggedinuser' facebook-logo='true'</fb:profile-pic>"
	       +"Welcome, <fb:name uid='loggedinuser' useyou='false'></fb:name>"
	       +"you are Signed in through your facebook account </span>";
 statusChangeCallback(response);
});

// Load the SDK asynchronously
(function(d, s, id) {
 var js, fjs = d.getElementsByTagName(s)[0];
 if (d.getElementById(id)) return;
 js = d.createElement(s); js.id = id;
 js.src = "//connect.facebook.net/en_US/sdk.js";
 fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

// Here we run a very simple test of the Graph API after login is
// successful.  See statusChangeCallback() for when this call is made.
function testAPI() {
 console.log('Welcome!  Fetching your information.... ');
 FB.api('/me', function(response) {
 	 alert(response.name);
 	console.log(JSON.stringify(response));
   console.log('Successful login for: ' + response.name);
   document.getElementById('details').innerHTML =
     'Thanks for logging in, ' + response.name + '!';
 });
}
</script>
<div id="firstheader">
<img src="img/Bubble.png" id="icon">
<label id="firstlabel">Kenscio Chat</label>
</div>

<div id="secondheader">
<div id="top">
  <label id="secondlabel">Kenscio Live Chat</label><label id="min" title="minimize">-</label>
  </div>
  <div id="content">
   <label id="intro">Introduce yourself *</label>
  <div id="fakeinput">
    <label id="fakeclick">Name, Email</label><label style="margin-left:90px;align:center">or</label><img src="img/faceb.jpg" style="width:20px;height:20px;margin-left:10px;margin-top:5px;margin-bottom: -5px;cursor:pointer;"><img src="img/google.jpg" style="width:20px;cursor:pointer;height:20px;margin-left:5px;margin-top:5px;margin-bottom: -5px">   
    </div>
    
     <form action="verify" onsubmit="return verifydata();">
  <div id="namemail">
     <div id="nameshow">
     <input type="text" name="name" placeholder="Enter Name" id="name">
     <input type="text" name="email" placeholder="Enter Email" id="email">
    <div style="padding-top:10px">
     <label style="margin-left:10px;margin-top:10px;color:#AAA; ">or sign in with</label><fb:login-button  data-size="icon"  onlogin="checkLoginState();" scope="public_profile,email"></fb:login-button> <img src="img/google.jpg" class="google"/>  
      </div></div>
      <!-- <label style="margin-left:10px;padding-top:5px">Choose a Department *</label>
      <select name="department" class="selectdept" id="department">
     <option name="department" value="select">select</option>
     <option name="department" value="billing">Billing</option>
      <option name="department" value="billing">general</option>
     </select> -->
     <div style="padding-top:10px">
     <label style="margin-left:10px;margin-top:10px">Message</label><br>
     <div id="textarea">
    <textarea name="message" rows="3" cols="28" class="textarea"></textarea>
        </div></div></div>
 </div>  
   <div class="submitarea">
  <input type="submit" value="Start Chatting" id="chat">
   
  </div> </form>  
</div>
<div id="fb-root"></div>
</body>
</html>