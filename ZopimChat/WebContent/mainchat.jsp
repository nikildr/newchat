<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
    	   $("#pastchatdisplay").hide();
    	   $("#currentchat").css({"color":"white","background-color":"#a8281f"});
    	   $("#rightcontent").css("margin-top", "-416px");
    	   $("#search").hide();
    	$("#currentchat").click(function(){
    		 $("#currentchatdisplay").show();
    		 $("#pastchatdisplay").hide();
    		 $("#currentchat").css({"color":"white","background-color":"#a8281f"});
    		 $("#pastchat").css({"color":"black","background-color":"#f8f5ef"});
    		 $("#rightcontent").css("margin-top", "-416px");
    		 $("#search").hide();
    	});
    	
    	 $("#pastchat").click(function(){
   		    $("#pastchatdisplay").show();
   		    $("#currentchatdisplay").hide();
   		    $("#pastchat").css({"color":"white","background-color":"#a8281f"});
   		    $("#currentchat").css({"color":"black","background-color":"#f8f5ef"});
   		    $("#rightcontent").css("margin-top", "-402px");
   		    $("#search").show();
   	});
    	
    });
 
</script>
</head>
<style>
#mainheader{margin-left:50px; margin-top:30px;border-top-left-radius:5px;border-top-right-radius:5px;background-color:#a8281f;height:40px;width:800px;}
#left{float:left;margin-top:5px;color:white; }
#right{float:right;margin-right:10px;margin-top:8px; }
.selectdept{width:110px;height:20px;border:1px solid #DDD;border-radius:5px;}
#container{height:500px;width:800px;background-color:#f8f5ef;margin-left:50px}
#chatdisplay{height:300px;width:488px;background-color: white;border: 1px solid #d9d9d9;margin-left:30px;margin-top:10px; }
#sendchat{bottom:2px;margin-left:30px;margin-top:10px;}
#pastchatdisplay{height:400px;width:488px;background-color: white;border: 1px solid #d9d9d9;margin-left:30px;margin-top:10px;}
#rightcontent{height:400px;width:220px;background-color:white;border:1px solid #d9d9d9;float:right;margin-right:15px;
padding-top:10px;padding-left:10px;position:relative;}
#search{height:18px;width:150px;border-radius:5px;  }
</style>
<body>
<div id="mainheader">
<div id="left">
<span style="margin-left:10px;"><label>UserName:</label></span>

<span><img src="img/chr.jpg" style="width:15px;margin-top:5px;margin-bottom: -2px"></span><span><img src="img/india.png" style="margin-left:5px;width:15px;height:15px;margin-bottom:-2px"></span><span><img src="img/windows.jpg" style="margin-left:5px;width:15px;height:15px;margin-bottom: -2px"></span>
</div>
<div id="right">
 <span style="margin-top:5px"><select name="action" class="selectdept">
  <option>Actions</option>
  <option name="action" value="Export Transcript">Export Transcript</option>
  <option name="action" value="Ban Visitor">Ban Visitor</option>
  <option name="action" value="Translate Chat">Translate Chat</option>
 </select></span>
 <span><img src="img/min.png" style="height:20px;width:20px;margin-bottom:-4px"></span><span><img src="img/close.jpg" style="height:20px;width:20px;margin-bottom:-4px;background-color:#a8281f "></span>
</div>
</div>
<div id="container">
<div id="buttons" style="padding-top:10px">
  <span style="width:90px;height:25px;"><label id="currentchat"style="margin-left:30px;cursor:pointer;text-align:center;font-size:medium;font-weight:normal;border-top-left-radius:2px;border-bottom-left-radius:2px;border:1px solid #d9d9d9;">Current Chat</label></span>
  <span style="width:90px;height:25px;"><label id="pastchat"style="margin-left:-3px;border-top-right-radius:2px;text-align:center;font-size:medium;font-weight:normal;cursor:pointer;border-bottom-right-radius:2px;width:90px;height:25px;border:1px solid #d9d9d9">Past Chat</label></span>
  <span style="padding-left: 190px"><input type="text" name="search" id="search" placeholder="Find a Conversation"></span>

</div>
<div id="currentchatdisplay">
  <div id="chatdisplay">
  </div>
  <div id="sendchat">
    <textarea rows="6" cols="58" style="border-radius:2px;border: 1px solid #d9d9d9"></textarea>
  </div>
  </div>
  <div id="pastchatdisplay" style="display:none">
  
  </div>
  <div id="rightcontent">
   <label style="color:#AAA">LOCATION</label><br>
   <label>India</label><br>
   <label style="color:#AAA">BROWSER</label><br>
   <label>Chrome 34.0.1847.131</label><br>
   <label style="color:#AAA">PLATFORM</label><br>
   <label>Windows 7</label><br>
   <label style="color:#AAA">IP ADDRESS</label><br>
   <label>175.100.150.102</label><br>
     <label style="color:#AAA">HOSTNAME</label><br>
   <label>102-150-100-175.static.youbroadband.in</label>
  
   </div>
</div>
</body>
</html>