<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>kenscio-chat</title>
<link class="jsbin" href="themes/blitzer/minified/jquery-ui.min.css" rel="stylesheet" type="text/css" />
<link type="image/icon" href="/img/logo_link.jpg" rel="shortcut icon"/>

  <script  src="js/jquery-1.9.1.js"></script>
  <script  src="js/jquery-ui.js"></script>
  <script type="text/javascript" src="js/jquery.dialogextend.js"></script>
  <style>
#mainheader{margin-left:50px; margin-top:30px;border-top-left-radius:5px;border-top-right-radius:5px;background-color:#a8281f;height:40px;width:800px;}
#left{float:left;margin-top:5px;color:white; }
#right{float:right;margin-right:10px;margin-top:8px; }
.selectdept{width:110px;height:20px;border:1px solid #DDD;border-radius:5px;}
#container{height:100%;width:100%;background-color:#f8f5ef;margin-left:0px}
#chatdisplay{height:300px;width:600px;background-color: white;border: 1px solid #d9d9d9;margin-left:30px;margin-top:10px; }
#sendchat{bottom:2px;margin-left:30px;margin-top:10px;}
#pastchatdisplay{height:420px;width:600px;background-color: white;border: 1px solid #d9d9d9;margin-left:30px;margin-top:10px;}
#rightcontent,#behaviouraldis,#transactiondis{height:405px;width:260px;background-color:white;border:1px solid #d9d9d9;float:right;margin-right:55px;
padding-top:10px;position:relative;}
#search{height: 20px;width: 180px;border-radius: 8px;margin-left: 380px;margin-top: -26px;border: 1px solid #cccccc;  }

#currentchat{margin-left:30px;cursor:pointer;text-align:center;font-size:11px;font-weight:boldl;
border-top-left-radius:4px;border-bottom-left-radius:4px;border:1px solid silver;color: black;line-height: 22px;
font-size: 11px;font-weight: bold;text-shadow: 0 1px 1px rgba(255,255,255,.5);}

#pastchat{margin-left:-7px;border-top-right-radius:4px;text-align:center;color:black;
cursor:pointer;border-bottom-right-radius:4px;border:1px solid silver;color: black;line-height: 22px;
font-size: 11px;font-weight: bold;text-shadow: 0 1px 1px rgba(255,255,255,.5);}

#new-dialog{cursor:pointer;text-align:center;font-size:medium;font-weight:normal;
border-top-left-radius:2px;border-bottom-left-radius:2px;border:1px solid #d9d9d9;color: #666666;line-height: 22px;
font-size: 11px;font-weight: bold;text-shadow: 0 1px 1px rgba(255,255,255,.5);}
label{text-align:center}
#demographic{margin-left:570px;border-top-left-radius:4px;text-align:center;cursor:pointer;border-bottom-left-radius:4px;border:1px solid buttonface;border:1px solid silver;color:black;line-height: 22px;
font-size: 11px;font-weight: bold;text-shadow: 0 1px 1px rgba(255,255,255,.5);background-color:buttonface;}
#transaction{margin-left:-7px;text-align:center;cursor:pointer;border:1px solid silver;border:1px solid silver;color:black;line-height: 22px;
font-size: 11px;font-weight: bold;text-shadow: 0 1px 1px rgba(255,255,255,.5);background-color:buttonface;}
#behavioural{margin-left:-7px;border-top-right-radius:4px;text-align:center;cursor:pointer;border-bottom-right-radius:4px;border:1px solid silver;color:black;line-height: 22px;
font-size: 12px;font-weight: bold;text-shadow: 0 1px 1px rgba(255,255,255,.5);background-color:buttonface;}
</style>
<script type="text/javascript">
    $(document).ready(function(){
    	   $("#pastchatdisplay").hide();
    	   $("#transactiondis").hide();
  		   $("#behaviouraldis").hide();
    	   $("#currentchat").css({"color":"white","background-color":"#c00"}); 
    	   $("#demographic").css({"color":"white","background-color":"#c00"});
    	   $("#pastchat").css({"color":"black","background-color":"buttonface"});
    	   $("#rightcontent").css("margin-top", "-430px");
    	   $("#search").hide();
    	  /*   $("button").mouseover(function(){
    		  $(this).css({"background-color":"red","color":"white"}); 
    	   });
    	   $("button").mouseleave(function(){
     		  $(this).css({"background-color":"#f8f5ef","color":"#666666"}); 
     	   });  */
    	$("#currentchat").click(function(){
    		 $("#currentchatdisplay").show();
    		 $("#pastchatdisplay").hide();
    		 $("#currentchat").css({"color":"white","background-color":"#c00"});
    		 $("#pastchat").css({"color":"black","background-color":"buttonface"});
    		 $("#rightcontent,#transactiondis,#behaviouraldis").css("margin-top", "-430px");
    		 $("#search").hide();
    	});
    	 $("#pastchat").click(function(){
   		    $("#pastchatdisplay").show();
   		    $("#currentchatdisplay").hide();
   		    $("#pastchat").css({"color":"white","background-color":"#c00"});
   		    $("#currentchat").css({"color":"black","background-color":"buttonface"});
   		  $("#rightcontent,#transactiondis,#behaviouraldis").css("margin-top", "-420px");
   		    $("#search").show();
    	 });   
   		$("#demographic").click(function(){
   			$("#rightcontent").show();
   			$("#transactiondis").hide();
   			$("#behaviouraldis").hide();
   		    $("#demographic").css({"color":"white","background-color":"#c00"});
   		    $("#transaction").css({"color":"black","background-color":"buttonface"});
   		   $("#behavioural").css({"color":"black","background-color":"buttonface"});
   		    $("#rightcontent").css("margin-top", "-420px");
   		});
   		$("#transaction").click(function(){
   			$("#rightcontent").hide();
   			$("#transactiondis").show();
   			$("#behaviouraldis").hide();
   		 $("#transaction").css({"color":"white","background-color":"#c00"});
   		$("#demographic").css({"color":"black","background-color":"buttonface"});
		   $("#behavioural").css({"color":"black","background-color":"buttonface"});
   		    $("#transactiondis").css("margin-top", "-420px");
   		});
   		$("#behavioural").click(function(){
   			$("#rightcontent").hide();
   			$("#transactiondis").hide();
   			$("#behaviouraldis").show();
   			$("#demographic").css({"color":"black","background-color":"buttonface"});
 		   $("#transaction").css({"color":"black","background-color":"buttonface"});
   		 $("#behavioural").css({"color":"white","background-color":"#c00"});
   			$("#behaviouraldis").css("margin-top", "-420px");
   		});
            //var last;
    	 $("#new-dialog").click(function(){
    		/*  openpopbox(); 
    		 $dialog('open');
    		 return false; */
    	 
    	 /* $("#dialog1").click(function(){
    		 openpopbox1(); 
    		 return false;
    	 }); */
   	 
  /* function openpopbox(){ */		 
    	//dialog option
   var dialogOptions = {
    "title" :"Dash Board" +new Date(),
    "width" : 1100,
    	 "height" : 550,
     "modal" : false,
    "resizable" :true, 
    "draggable" : true,
    "autoOpen":true
    	 };
var dialogExtendOptions = {
    "closable" :true,
   "maximizable" :false,
     "minimizable" :true,
  "minimizeLocation":"right", 
    "collapsable" : false,
     "dblclick" : false, 
     "icons" : {
    		"close" : "ui-icon-circle-close",
    		 "maximize" : "ui-icon-circle-plus",
    		"minimize" : "ui-icon-circle-minus",
     },
 };
  $("#container").dialog(dialogOptions).dialogExtend(dialogExtendOptions);
  });
    	 }); 
  function openpopbox1(){		 
	 //dialog option
    		    var dialogOptions = {
    		      "title" :"Dash Board" +new Date(),
    		      "width" : 1100,
    		      "height" : 550,
    		      "modal" : false,
    		      "resizable" :true, 
    		      "draggable" : true,
    		    };
    		    var dialogExtendOptions = {
    		    	      "closable" :true,
    		    	      "maximizable" :false,
    		    	      "minimizable" :true,
    		    	       "minimizeLocation":"right", 
    		    	      "collapsable" : false,
    		    	      "dblclick" : false, 
    		    	      "icons" : {
    		    	          "close" : "ui-icon-circle-close",
    		    	          "maximize" : "ui-icon-circle-plus",
    		    	          "minimize" : "ui-icon-circle-minus",
    		    	        },
    		    	    };
    	$("#dialogcontent").dialog(dialogOptions).dialogExtend(dialogExtendOptions);
};
 
 </script>
</head>
<body>
<button type="button" id="new-dialog">Click Me!!</button>
<button type="button" id="dialog1">Click Me!!</button>
  <div id="container" style="display:none">
<div id="buttons" style="padding-top:10px">
  <span><button id="currentchat">Current Chat</button></span>
  <span><button id="pastchat">Past Chat</button></span>
  <span><button id="demographic">Demographic</button></span>
  <span><button id="transaction">Transaction</button></span>
  <span><button id="behavioural">Behavioural</button></span>
 <!--  <span><input type="text" name="search" id="search" placeholder="Find a Conversation"></span> -->
</div>
<div id="currentchatdisplay">
  <div id="chatdisplay">
  </div>
  <div id="sendchat">
    <textarea rows="5" cols="72" style="border-radius:2px;border: 1px solid #d9d9d9"></textarea>
  </div>
  </div>
  <div id="pastchatdisplay" style="display:none">
  
  </div>
  <div id="rightcontent">
  <table>
<tr><td style="color:#AAA;text-align:center">LOCATION</td></tr>
<tr><td style="color:black;text-align:center">India</td></tr>
<tr><td style="color:#AAA;text-align:center">BROWSER</td></tr>
<tr><td style="color:black;text-align:center">Chrome 34.0.1847.131</td></tr>
<tr><td style="color:#AAA;text-align:center">PLATFORM 7</td></tr>
<tr><td style="color:black;text-align:center">Windows 7</td></tr>
<tr><td style="color:#AAA;text-align:center">IP ADDRESS</td></tr>
<tr><td style="color:black;text-align:center">175.100.150.102</td></tr>
<tr><td style="color:#AAA;text-align:center">HOSTNAME</td></tr>
<tr><td style="color:black;text-align:center">102-150-100-175.static.youbroadband.in</td></tr></table>
</div>
   <div id="transactiondis">
    <h4>transaction</h4></div>
  <div id="behaviouraldis">
  <h4>behavioural</h4>
  </div>
  </div>
  <div id="dialogcontent" style="display:none">
  <h1>hai</h1>
  </div>
</body>
</html>