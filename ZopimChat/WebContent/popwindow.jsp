<!DOCTYPE html>
<html>
<head>
  <title>kenscio-chat</title>
  <meta charset=utf-8 />
  <link class="jsbin" href="themes/blitzer/minified/jquery-ui.min.css" rel="stylesheet" type="text/css" />
  <script class="jsbin" src="js/jquery-1.9.1.js"></script>
  <script class="jsbin" src="js/jquery-ui.js"></script>
  <script type="text/javascript" src="js/jquery.dialogextend.js"></script>
  <style>
#mainheader{margin-left:50px; margin-top:30px;border-top-left-radius:5px;border-top-right-radius:5px;background-color:#a8281f;height:40px;width:800px;}
#left{float:left;margin-top:5px;color:white; }
#right{float:right;margin-right:10px;margin-top:8px; }
.selectdept{width:110px;height:20px;border:1px solid #DDD;border-radius:5px;}
#container{height:100%;width:100%;background-color:#f8f5ef;margin-left:0px}
#chatdisplay{height:300px;width:488px;background-color: white;border: 1px solid #d9d9d9;margin-left:30px;margin-top:10px; }
#sendchat{bottom:2px;margin-left:30px;margin-top:10px;}
#pastchatdisplay{height:400px;width:488px;background-color: white;border: 1px solid #d9d9d9;margin-left:30px;margin-top:10px;}
#rightcontent{height:400px;width:220px;background-color:white;border:1px solid #d9d9d9;float:right;margin-right:15px;
padding-top:10px;padding-left:10px;position:relative;}
#search{height:18px;width:150px;border-radius:5px;  }
#currentchat{margin-left:30px;cursor:pointer;text-align:center;font-size:medium;font-weight:normal;
border-top-left-radius:2px;border-bottom-left-radius:2px;border:1px solid #d9d9d9;color: #666666;line-height: 22px;
font-size: 11px;font-weight: bold;text-shadow: 0 1px 1px rgba(255,255,255,.5);}
#pastchat{margin-left:-6px;border-top-right-radius:2px;text-align:center;
cursor:pointer;border-bottom-right-radius:2px;border:1px solid #d9d9d9;border:1px solid #d9d9d9;color: #666666;line-height: 22px;
font-size: 11px;font-weight: bold;text-shadow: 0 1px 1px rgba(255,255,255,.5);}
#new-dialog{cursor:pointer;text-align:center;font-size:medium;font-weight:normal;
border-top-left-radius:2px;border-bottom-left-radius:2px;border:1px solid #d9d9d9;color: #666666;line-height: 22px;
font-size: 11px;font-weight: bold;text-shadow: 0 1px 1px rgba(255,255,255,.5);}
</style>
<script type="text/javascript">
    $(document).ready(function(){
    	   $("#pastchatdisplay").hide();
    	   $("#currentchat").css({"color":"white","background-color":"#c00"});
    	   $("#pastchat").css({"color":"black","background-color":"#f8f5ef"});
    	   $("#rightcontent").css("margin-top", "-416px");
    	   $("#search").hide();
    	$("#currentchat").click(function(){
    		 $("#currentchatdisplay").show();
    		 $("#pastchatdisplay").hide();
    		 $("#currentchat").css({"color":"white","background-color":"#c00"});
    		 $("#pastchat").css({"color":"black","background-color":"#f8f5ef"});
    		 $("#rightcontent").css("margin-top", "-416px");
    		 $("#search").hide();
    	});
    	 $("#pastchat").click(function(){
   		    $("#pastchatdisplay").show();
   		    $("#currentchatdisplay").hide();
   		    $("#pastchat").css({"color":"white","background-color":"#c00"});
   		    $("#currentchat").css({"color":"black","background-color":"#f8f5ef"});
   		    $("#rightcontent").css("margin-top", "-402px");
   		    $("#search").show();
   	});
    	 var last;
    	 $("#new-dialog").click(function(){
    		    //dialog options
    		    var dialogOptions = {
    		      "title" :"Dash Board ",
    		      "width" : 1100,
    		      "height" : 550,
    		      "modal" : true,
    		      "resizable" :true, 
    		      "draggable" : true,
    		    };
    		    var dialogExtendOptions = {
    		    	      "closable" :true,
    		    	      "maximizable" :false,
    		    	      "minimizable" :true,
    		    	      /* "minimizeLocation" : $("#my-form [name=minimizeLocation]:checked").val() || false,
    		    	      "collapsable" : $("#button-collapse").is(":checked"),
    		    	      "dblclick" : $("#my-form [name=dblclick]:checked").val() || false, */
    		    	      "icons" : {
    		    	          "close" : "ui-icon-circle-close",
    		    	          "maximize" : "ui-icon-circle-plus",
    		    	          "minimize" : "ui-icon-circle-minus",
    		    	        },
    		    	    };
    		    last = $("#container").dialog(dialogOptions).dialogExtend(dialogExtendOptions);
    	  });
    });
 
</script>
</head>
<body>
  <button type="button" id="new-dialog">Click Me!!</button>
  <div id="container" style="display:none">
<div id="buttons" style="padding-top:10px">
  <span><button id="currentchat">Current Chat</button></span>
  <span><button id="pastchat">Past Chat</button></span>
  <span style="padding-left: 180px"><input type="text" name="search" id="search" placeholder="Find a Conversation"></span>
</div>
<div id="currentchatdisplay">
  <div id="chatdisplay">
  </div>
  <div id="sendchat">
    <textarea rows="6" cols="58" style="border-radius:2px;border: 1px solid #d9d9d9"></textarea>
  </div>
  </div>
  <div id="pastchatdisplay" style="display:none">
  
  </div>
  <div id="rightcontent">
   <label style="color:#AAA">LOCATION</label><br>
   <label>India</label><br>
   <label style="color:#AAA">BROWSER</label><br>
   <label>Chrome 34.0.1847.131</label><br>
   <label style="color:#AAA">PLATFORM</label><br>
   <label>Windows 7</label><br>
   <label style="color:#AAA">IP ADDRESS</label><br>
   <label>175.100.150.102</label><br>
     <label style="color:#AAA">HOSTNAME</label><br>
   <label>102-150-100-175.static.youbroadband.in</label>
  </div></div>
</body>
</html>